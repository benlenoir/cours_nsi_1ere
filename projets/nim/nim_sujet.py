#---------------------------------------------
#  Importation des modules
#---------------------------------------------


#----------------------------------------------
# Fonctions d'affichage
#----------------------------------------------
def affiche_joueur(joueur_actuel):
    pass


def affiche_situation(situation):
    pass


def affiche_choix(choix, joueur_actuel) :
    pass


def affiche_coups(coups_max):
    pass 


def affiche_fin_partie(joueur_actuel):
    pass


#----------------------------------------------
# Fonctions relatives aux joueurs
#-----------------------------------------------

def premier_joueur(joueur1, joueur2):
    pass


def change_joueur(joueur_actuel, joueur1,  joueur2):
    pass

    
#----------------------------------------------
# Fonctions liées aux coups joués
#----------------------------------------------

def existe_coup(situation):
    pass


def coups_possibles(situation):
    pass

    
def choix_coups(coups_max):
    pass


def msj_situation(choix, situation):
    pass


#---------------------------------------------
# Fonction principale
#---------------------------------------------

def jouer():
    pass




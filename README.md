# Cours de 1ère NSI 2020/2021 

Projet regroupant l'ensemble des cours dispensés en 1ère NSI 2020/2021

* La plupart des documents proposés sur ce site sont sous licence creative commons : BY-NC-SA.

* Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre.
Merci de respecter les conditions de la licence

Pour tous les autres : les sources sont citées

--- 

# Mini-Projet: Jeu de Nim (à rendre pour le 19/01/21)

* [Enonce](./projets/nim/nim_enonce.md)   
  
* [Script support](./projets/nim/nim_sujet.py)   

---
